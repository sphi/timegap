# timegap (`tg`)
A tool to add timestamps to output lines of an arbitrary program and/or display the duration of gaps between output lines.
```
$ tg bash -c 'echo foo; sleep 2.5; echo bar'
0.002: foo
     - 2.502s -
2.504: bar
```

Use the `-g` flag to only show gaps.
```
$ tg -g sudo dmesg -w
```

`tg` can run a command, or accept piped input.
```
$ journalctl --user -f | tg
```

See `tg --help` for all options.
