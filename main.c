#define _XOPEN_SOURCE 600

#include <unistd.h>
#include <poll.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/ioctl.h>

// Should never be <1. Gaps will only be printed if larger than this.
int threshold_ms = 200;
int update_ms = 47; // Randomish number to make output look noisy
bool color_output = false;
bool enable_time_gaps = false;
bool enable_timestamps = false; // These both get set to true if neither are set explicitly
bool print_realtime = true; // If to update the timestamp (requires ability to clear terminal lines)
pid_t subprocess_pid = 0;
int program_input_fd = -1;
bool assume_input_echos = true;
int program_output_fd = STDIN_FILENO;

double program_start_time = 0;
double gap_start_time = 0;
double current_time = 0;
FILE* output_file = NULL;
struct termios* original_term_attrs = NULL;

enum line_contents_t {
    LINE_FRESH,
    LINE_HAS_OUTPUT,
    LINE_HAS_INPUT,
    LINE_HAS_TIMESTAMP,
    LINE_HAS_TIME_GAP,
} current_line_contents = LINE_FRESH;

enum poll_result_t {
    POLL_RESULT_PROGRAM_OUTPUT,
    POLL_RESULT_INPUT,
    POLL_RESULT_TIMEOUT,
    POLL_RESULT_DONE,
    POLL_RESULT_ERROR,
};

void refresh_current_time(void) {
    struct timespec ts;
    // CLOCK_BOOTTIME instead of CLOCK_MONOTONIC so time spent in suspend is measured
    clock_gettime(CLOCK_BOOTTIME, &ts);
    current_time = ts.tv_sec + ts.tv_nsec / 1000000000.0;
}

void clear_line(void) {
    if (print_realtime) {
        // Erase current line and go to start
        fprintf(output_file, "\33[2K\r");
    } else {
        // This generally shouldn't happen
        fprintf(output_file, "\n");
    }
}

// current_time should always be ~get_timestamp(), sending it in is just an optimization in case this function is called
// many times at at about the same time
void print_time(bool timestamp, bool gap) {
    if (!timestamp && !gap) {
        return;
    }
    switch (current_line_contents) {
    case LINE_HAS_OUTPUT:
    case LINE_HAS_INPUT:
        fprintf(output_file, "\n");
        break;

    case LINE_HAS_TIMESTAMP:
    case LINE_HAS_TIME_GAP:
        clear_line();
        break;

    default:;
    }

    if (!gap) {
        // Only timestamps
        fprintf(
            output_file,
            "%s%.3f:%s ",
            color_output ? "\33[34m" : "",
            current_time - program_start_time,
            color_output ? "\33[0m" : ""
        );
    } else if (!timestamp) {
        // Only time gaps
        fprintf(
            output_file,
            "%s %s- %.3fs - %s",
            color_output ? "\33[35m" : "",
            enable_timestamps ? "    " : "",
            current_time - gap_start_time,
            color_output ? "\33[0m" : "");
    } else {
        // Both combined on one line
        fprintf(
            output_file,
            "%s%.3f:%s %.3fs%s",
            color_output ? "\33[34m" : "",
            current_time - program_start_time,
            color_output ? "\33[35m" : "",
            current_time - gap_start_time,
            color_output ? "\33[0m" : ""
        );
    }

    fflush(output_file);
    current_line_contents = gap ? LINE_HAS_TIME_GAP : LINE_HAS_TIMESTAMP;
}

void write_program_output(const char* buf, ssize_t size) {
    if (size) {
        write(fileno(output_file), buf, size);
        switch (buf[size - 1]) {
            case '\n': case '\r': current_line_contents = LINE_FRESH;      break;
            default:              current_line_contents = LINE_HAS_OUTPUT; break;
        }
    }
}

void handle_program_output(const char* buf, ssize_t size) {
    if (!size) {
        // Should never happen
        return;
    }
    if (current_line_contents == LINE_HAS_TIME_GAP) {
        write(fileno(output_file), "\n", 1);
        current_line_contents = LINE_FRESH;
    }
    if (enable_timestamps) {
        const char* line_start = buf;
        while (line_start < buf + size) {
            if (current_line_contents != LINE_HAS_OUTPUT) {
                print_time(true, false);
            }
            const char* line_end = line_start;
            while (line_end < buf + size) {
                char c = *line_end;
                line_end++;
                if (c == '\n' || c == '\r') {
                    break;
                }
            }
            if (line_end > line_start) {
                write_program_output(line_start, line_end - line_start);
            }
            line_start = line_end;
        }
    } else {
        write_program_output(buf, size);
    }
}

void handle_input(const char* buf, ssize_t size) {
    if (program_input_fd >= 0) {
        write(program_input_fd, buf, size);
    }
    if (size && assume_input_echos) { // Should always be true
        current_line_contents = buf[size - 1] == '\n' ? LINE_FRESH : LINE_HAS_INPUT;
    }
}

// Returns false on error
bool read_available_data(int fd, void (*handler)(const char* buf, ssize_t size)) {
    const ssize_t buf_size = 1024;
    char buf[buf_size];
    bool succeeded = true;
    ssize_t read_len = 0;
    do {
        read_len = read(fd, buf, buf_size);
        if (read_len < 0) {
            if (errno != EAGAIN && errno != EWOULDBLOCK) {
                succeeded = false;
            }
        } else if (read_len == 0) {
            succeeded = false;
        } else {
            handler(buf, read_len);
        }
    } while (read_len == buf_size);
    fflush(output_file);
    return succeeded;
}

enum poll_result_t poll_fds(int timeout_ms)
{
    struct pollfd pfd[] = {
        { program_output_fd, POLLIN, 0 },
        { STDIN_FILENO, POLLIN, 0 },
    };
    int fd_count = program_output_fd == STDIN_FILENO ? 1 : 2;
    int const ret = poll(pfd, fd_count, timeout_ms);
    if (ret > 0 && pfd[0].revents & POLLIN) {
        // Something to read from the program
        return POLL_RESULT_PROGRAM_OUTPUT;
    } else if (ret > 0 && pfd[1].revents & POLLIN) {
        // Something to read from stdin
        return POLL_RESULT_INPUT;
    } else if (ret > 0) {
        // Something has closed, we're done
        return POLL_RESULT_DONE;
    } else if (ret == 0) {
        // nothing to read
        return POLL_RESULT_TIMEOUT;
    } else {
        fprintf(stderr, "tg error polling input: %s\n", strerror(errno));
        return POLL_RESULT_ERROR;
    }
}

void set_fd_as_nonblocking(int fd) {
    int flags = fcntl(fd, F_GETFL, 0);
    fcntl(fd, F_SETFL, flags | O_NONBLOCK);
}

void maybe_print_gap(void) {
    if (current_line_contents == LINE_HAS_TIME_GAP ||
        threshold_ms == 1 ||
        (current_time - gap_start_time) > (threshold_ms / 1000.0)
    ) {
        print_time(false, enable_time_gaps);
    }
}

// Returns false on error
bool run(void) {
    set_fd_as_nonblocking(program_output_fd);
    if (program_output_fd != STDIN_FILENO) {
        set_fd_as_nonblocking(STDIN_FILENO);
    }
    refresh_current_time();
    program_start_time = current_time;
    gap_start_time = current_time;
    int next_sleep = threshold_ms;
    while (true) {
        if (!print_realtime) {
            next_sleep = -1;
        }
        enum poll_result_t poll_result = poll_fds(next_sleep);
        refresh_current_time();
        switch (poll_result)
        {
        case POLL_RESULT_PROGRAM_OUTPUT:
            maybe_print_gap();
            if (!read_available_data(program_output_fd, handle_program_output)) {
                fprintf(stderr, "tg error reading program output\n");
                return false;
            }
            next_sleep = threshold_ms;
            gap_start_time = current_time;
            break;

        case POLL_RESULT_INPUT:
            if (assume_input_echos) {
                current_line_contents = LINE_HAS_INPUT;
            }
            if (!read_available_data(STDIN_FILENO, handle_input)) {
                 fprintf(stderr, "tg error reading stdin\n");
                 return false;
            }
            next_sleep = current_line_contents == LINE_HAS_INPUT ? -1 : threshold_ms;
            if (assume_input_echos) {
                gap_start_time = current_time;
            }
            break;

        case POLL_RESULT_TIMEOUT:
            if (print_realtime && current_line_contents != LINE_HAS_INPUT) {
                print_time(enable_timestamps && !enable_time_gaps, enable_time_gaps);
            }
            next_sleep = update_ms;
            break;

        case POLL_RESULT_DONE:
            maybe_print_gap();
            if (current_line_contents == LINE_HAS_TIME_GAP ||
                current_line_contents == LINE_HAS_TIMESTAMP
            ) {
                fprintf(output_file, "\n");
            }
            return true;

        case POLL_RESULT_ERROR:
        default:
            return false;
        }
    }
}

// Returns false on error
bool run_subprocess(char *const cmd[]) {
    original_term_attrs = malloc(sizeof(struct termios));
    struct winsize term_size = {0};
    if (tcgetattr(STDIN_FILENO, original_term_attrs) < 0) {
        if (errno == 25) {
            // This happens when stdin is a pipe
            free(original_term_attrs);
            assume_input_echos = false;
            original_term_attrs = NULL;
        } else {
            fprintf(stderr, "tg getting terminal attributes: %s\n", strerror(errno));
            return false;
        }
    } else {
        if (ioctl(STDIN_FILENO, TIOCGWINSZ, &term_size) < 0) {
            fprintf(stderr, "tg getting terminal size: %s\n", strerror(errno));
            return false;
        }
    }
    int master_fd = posix_openpt(O_RDWR | O_NOCTTY);
    if (master_fd < 0) {
        fprintf(stderr, "tg creating master fd: %s\n", strerror(errno));
        return false;
    }
    if (grantpt(master_fd) < 0) {
        fprintf(stderr, "tg granting access to slave pty: %s\n", strerror(errno));
        return false;
    }
    if (unlockpt(master_fd) < 0) {
        fprintf(stderr, "tg unlocking slave pty: %s\n", strerror(errno));
        return false;
    }
    const char* slave_name = ptsname(master_fd);
    if (slave_name == NULL) {
        fprintf(stderr, "tg getting slave pty name: %s\n", strerror(errno));
        return false;
    }
    int slave_name_len = strlen(slave_name);
    char* slave_name_owned = malloc(slave_name_len + 1);
    strcpy(slave_name_owned, slave_name);
    subprocess_pid = fork();
    if (subprocess_pid == -1) {
        free(slave_name_owned);
        fprintf(stderr, "tg failed to fork: %s\n", strerror(errno));
        return false;
    } else if (subprocess_pid > 0) {
        free(slave_name_owned);
        if (original_term_attrs) {
            struct termios attrs;
            memcpy(&attrs, original_term_attrs, sizeof(struct termios));
            attrs.c_lflag &= ~ICANON;
            attrs.c_cc[VMIN] = 1;
            attrs.c_cc[VTIME] = 0;
            if (tcsetattr(STDIN_FILENO, TCSANOW, &attrs) < 0) {
                fprintf(stderr, "tg setting stdin terminal attributes: %s\n", strerror(errno));
            }
        }
        program_output_fd = master_fd;
        program_input_fd = master_fd;
        return true;
    } else {
        // We are the subprocess
        if (setsid() < 0) {
            fprintf(stderr, "tg setsid() failed: %s\n", strerror(errno));
            exit(1);
        }
        close(master_fd);
        int slave_fd = open(slave_name_owned, O_RDWR);
        if (slave_fd < 0) {
            fprintf(stderr, "tg creating slave fd %s: %s\n", slave_name_owned, strerror(errno));
            return false;
        }
        if (original_term_attrs) {
            original_term_attrs->c_lflag &= ~ECHO;
            if (tcsetattr(slave_fd, TCSANOW, original_term_attrs) < 0) {
                fprintf(stderr, "tg setting slave terminal attributes: %s\n", strerror(errno));
                exit(1);
            }
            if (ioctl(slave_fd, TIOCSWINSZ, &term_size) < 0) {
                fprintf(stderr, "tg setting slave terminal size: %s\n", strerror(errno));
                exit(1);
            }
        } else {
            struct termios attrs;
            if (tcgetattr(slave_fd, &attrs) < 0) {
                fprintf(stderr, "tg getting slave terminal attributes: %s\n", strerror(errno));
                return false;
            }
            attrs.c_lflag &= ~ECHO;
            if (tcsetattr(slave_fd, TCSANOW, &attrs) < 0) {
                fprintf(stderr, "tg setting slave terminal attributes: %s\n", strerror(errno));
                exit(1);
            }
        }
        if (dup2(slave_fd, STDIN_FILENO) != STDIN_FILENO) {
            fprintf(stderr, "tg duping stdin: %s\n", strerror(errno));
            exit(1);
        }
        if (dup2(slave_fd, STDOUT_FILENO) != STDOUT_FILENO) {
            fprintf(stderr, "tg duping stdout: %s\n", strerror(errno));
            exit(1);
        }
        if (dup2(slave_fd, STDERR_FILENO) != STDERR_FILENO) {
            fprintf(stderr, "tg duping stderr: %s\n", strerror(errno));
            exit(1);
        }
        close(slave_fd);
        execvp(cmd[0], cmd);
        fprintf(stderr, "tg failed to exec: %s\n", strerror(errno));
        exit(1);
    }
}

void show_help(void) {
    printf(
        "Usage: tg [OPTIONS]... [COMMAND]...\n"
        "Usage: COMMAND | tg [OPTIONS]...\n"
        "Display timing information of output lines of an arbitrary command\n"
        "\n"
        "Options:\n"
        "  -h, --help           show this help message and exit\n"
        "  -t, --timestamps     show only timestamps (time since program start) of each output line\n"
        "  -g, --gaps           show only gaps (duration of significant gaps between output lines)\n"
        "  -m, --min            minimum gap in miliseconds between output lines before displaying\n"
        "  -r, --realtime       always show realtime output (update times as they change)\n"
        "  -R, --no-realtime    never show realtime output\n"
        "  -c, --color          always show colored output\n"
        "  -C, --no-color       never show colored output\n"
    );
}

int main(int argc, const char* argv[]) {
    output_file = stdout;

    bool arg_error = false;
    bool happy_help = false;
    bool auto_color_output = true;
    bool auto_realtime = true;
    const char** subprocess_cmd = NULL;

    for (int i = 1; i < argc; i++) {
        const char* flag = argv[i];
        if (flag[0] == '-') {
            if (strcmp(flag, "-h") == 0 || strcmp(flag, "--help") == 0) {
                happy_help = true;
            } else if (strcmp(flag, "-t") == 0 || strcmp(flag, "--timestamps") == 0) {
                enable_timestamps = true;
            } else if (strcmp(flag, "-g") == 0 || strcmp(flag, "--gaps") == 0) {
                enable_time_gaps = true;
            } else if (strcmp(flag, "-m") == 0 || strcmp(flag, "--min") == 0) {
                if (i < argc - 1) i++;
                const char* end = argv[i];
                long gap_min = strtol(argv[i], (char**)&end, 10);
                if (argv[i] == flag) {
                    fprintf(stderr, "tg no argument for %s\n", flag);
                    arg_error = true;
                } else if (*end) {
                    fprintf(stderr, "tg invalid %s argument %s\n", flag, argv[i]);
                    arg_error = true;
                } else if (gap_min < 0) {
                    fprintf(stderr, "tg %s cannot be negative\n", flag);
                } else if (gap_min == 0) {
                    threshold_ms = 1;
                } else {
                    threshold_ms = gap_min;
                }
            } else if (strcmp(flag, "-r") == 0 || strcmp(flag, "--realtime") == 0) {
                print_realtime = true;
                auto_realtime = false;
            } else if (strcmp(flag, "-R") == 0 || strcmp(flag, "--no-realtime") == 0) {
                print_realtime = false;
                auto_realtime = false;
            } else if (strcmp(flag, "-c") == 0 || strcmp(flag, "--color") == 0) {
                color_output = true;
                auto_color_output = false;
            } else if (strcmp(flag, "-C") == 0 || strcmp(flag, "--no-color") == 0) {
                color_output = false;
                auto_color_output = false;
            } else {
                fprintf(stderr, "tg invalid argument: %s\n", flag);
                arg_error = true;
            }
        } else {
            subprocess_cmd = malloc((argc - i + 1) * sizeof(char*));
            for (int j = i; j < argc; j++) {
                subprocess_cmd[j - i] = argv[j];
            }
            subprocess_cmd[argc - i] = NULL;
            break;
        }
    }

    if (!enable_timestamps && !enable_time_gaps) {
        enable_timestamps = true;
        enable_time_gaps = true;
    }

    bool is_tty = isatty(fileno(output_file));
    if (auto_color_output) {
        color_output = is_tty;
    }
    if (auto_realtime) {
        print_realtime = is_tty && enable_time_gaps;
    }

    if (arg_error) {
        show_help();
        return 1;
    }

    if (happy_help) {
        show_help();
        return 0;
    }

    if (subprocess_cmd) {
        if (!run_subprocess((char** const)subprocess_cmd)) {
            exit(1);
        }
        free(subprocess_cmd);
    }

    bool ret = run();

    if (original_term_attrs) {
        // One might think SIGINT would need to be handled to make sure this runs, but it appears terminal modifications
        // don't stick in this case. Who knows.
        if (tcsetattr(STDIN_FILENO, TCSANOW, original_term_attrs) < 0) {
            fprintf(stderr, "tg failed to restore terminal attributes: %s\n", strerror(errno));
        }
        free(original_term_attrs);
    }

    return ret ? 0 : 1;
}
